\documentclass[10pt,a4paper,twoside]{book}
\usepackage[utf8]{inputenc}
\usepackage{fancyvrb,alltt,supertabular,fancyhdr,afterpage,multicol}
\usepackage[stable]{footmisc}
\usepackage[backref=page]{hyperref}
\usepackage[hmargin=4cm,vmargin=4.7cm]{geometry} 
\usepackage{pdfsync}

\RecustomVerbatimEnvironment 
 {Verbatim}{Verbatim} 
 {fontsize=\small} 
 
\usepackage{fontspec}
\usepackage{natbib}
  \bibpunct{[}{]}{,}{a}{}{;}
  %\bibpunct[];A{},
  %\let\cite=\citep

\usepackage{sectsty}
\allsectionsfont{\sffamily}

\makeatletter 
\def\iffloatpage#1#2{\if@fcolmade #1\else #2\fi} 
\makeatother 

%\makeatletter
%\renewcommand*\cleardoublepage{\clearpage\if@twoside
%  \ifodd\c@page \hbox{}\newpage\if@twocolumn\hbox{}%
%  \newpage\fi\fi\fi}
%\makeatother

\usepackage{color}
\definecolor{blue}{rgb}{0.6,0,0}
\definecolor{green}{rgb}{0,0.5,0}
\definecolor{cyan}{rgb}{0,0.8,0.8}

\usepackage[font={sf,small},skip=0.5cm]{caption} 

\usepackage{graphicx}
\usepackage{subfig}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{hyperref}
\hypersetup{
    colorlinks=true,       % false: boxed links; true: colored links
    linkcolor=blue,          % color of internal links
    citecolor=green,        % color of links to bibliography
    filecolor=blue,      % color of file links
    urlcolor=cyan          % color of external links
}

\usepackage{url}
\def\UrlFont{\rm}

\newcommand{\ii}[1]{{\it{#1}}}
\renewcommand{\>}{\rightarrow}
\newcommand{\<}{\leftarrow}
\newcommand{\A}{\uparrow}
\newcommand{\V}{\downarrow}
\renewcommand{\,}{{\cdot}}
\newcommand{\se}[1]{[\![{#1}]\!]}
\newcommand{\semf}[1]{\se{{#1}}_{E_\A}}
\newcommand{\sem}[1]{\se{{#1}}_{E_\V}}
\newcommand{\ser}[1]{\se{{#1}}_\rho}
\newcommand{\ang}[1]{\langle{#1}\rangle}
\newcommand{\ango}[1]{\langle{\ol{#1}}\rangle}
\newcommand{\ol}[1]{\overline{#1}}
\renewcommand{\tt}[1]{\texttt{#1}}
\newcommand{\mf}[1]{\mathsf{#1}}
\newcommand{\uv}[1]{“#1”}
\renewcommand{\|}{\ |\ }
\newcommand{\SUM}[1]{\displaystyle{\sum}_{{#1}}}
\renewcommand{\&}[1]{$&${#1}$&$}
\newcommand{\emp}{\ang{}}
\newcommand{\sub}{\ang{\square}}
\newcommand{\eqd}{\triangleq}
\newcommand{\ftt}{\textsc{Ftt}}

\newcommand{\figurefont}{\small}
\newcommand{\figurefonts}{\small}
\newcommand{\on}{\operatorname}
\newtheorem{thm}{Theorem}[chapter]
\newtheorem{defn}{Definition}
\newtheorem{lemma}[thm]{Lemma}
\newtheorem{example}[thm]{Example}
\newtheorem{definition}[defn]{Definition}


\newcommand{\refapp}[1]{\hyperref[appendix:#1]{Appendix~\ref*{appendix:#1}}}
\renewcommand{\labelitemi}{$\star$} 
\linespread{1.05}
\setcounter{tocdepth}{1}  
\begin{document}
\pagestyle{empty}

\include{title}
~\newpage
\pagenumbering{roman}
\chapter*{Acknowledgments\footnote{I try to give all the names in alphabetical order, 
  so noone gets disappointed for mentioning them in the second or the last 
  place.}\markboth{Acknowledgements}{Acknowledgements}}
  %\addcontentsline{toc}{chapter}{Acknowledgements}
    First of all I would like to thank Pierre Genev\`es, my
    internship tutor and his colleague Nabil Laya\"ıda, for their support and
    patience, with which they mentored me during my research program at INRIA
    Rh\^one Alpes.
    
    I really appreciated the presence, support and long hours of discussions of my closest friends – even though just 
    on the phone to the Czech Republic – Monika Nikolova and Petr Z\'atorsk\'y. Even on the distance
    they were always patient to listen to my experiences here in France and to get a different 
    point of view of the situations or moments experienced. 
    
    I would also like to thank my flatmates Amandine, Ma\"eva and Mathilde for their presence and support when my work was
    getting difficult and when I was not feeling very motivated. Also thanks to them I had the possibility to discover
    a little bit more of the french culture.
    
    In the end I want to thank my family, especially my grandparents, and Jacques
    Croset, who helped me to manage my study stay in France.

\chapter*{Abstract}
  %\addcontentsline{toc}{chapter}{Abstract}
    XQuery is an increasingly popular functional programming language, standardized
    by the World Wide Web Consortium, for querying, combining and producing XML
    data. The core language is based on the XPath expression, which allows
    navigating in XML documents, and on the FLWOR core language, which is
    designed to manipulate the structured data in a simple way. 
    
    XQuery is statically and strongly typed, i.e., types of input
    data and functions are defined in terms of regular expression types. However, 
    the current proposed type system suffers from poor precision as it heavily relies on
    over-approximations that lead to incomplete type-checkers. In particular, the
    type ordering for \tt{for} loops is lost, and most importantly, the type
    \ii{any} is always inferred for horizontal and upward vertical XPath axes.
    
    The objective of this master's thesis is to propose an alternative and
    more precise approach for typing XQuery. For this purpose, we introduce 
    a~new kind of bi-directional regular expression types:
    focused tree types, for supporting all XPath axes. We also follow the
    proposal of \cite{colazzo-icfp04} to type-check the \tt{for} expressions
    more precisely. We demonstrate the benefits of this much more precise type
    system on a~core fragment of XQuery, for which we propose new typing 
    rules that don't suffer from the analyzed flaws.

\section*{Keywords}
    XQuery, XML, Type systems, Functional languages
    
\chapter*{R\'esum\'e}

XQuery est un langage de programmation fonctionnel de plus en plus populaire
pour la programmation d’applications Web. Il est normalis\'e par le Consortium
World Wide Web et sert pour l’interrogation, la transformation et la production
de donn\'ees XML. Le langage de base est fond\'e sur XPath, qui permet la
navigation dans les documents XML au moyen d’expressions de chemins et le
langage se pr\'esente sous forme d’expressions dite FLWOR, qui sont con{\c c}ues pour
simplifier la manipulation des donn\'ees structur\'ees. 

Le langage XQuery est statiquement et fortement typ\'e, \`a savoir, les
donn\'ees manipul\'ees ainsi que les fonctions du langage sont d\'efinies en termes de types
construits au moyen d’expressions r\'eguli\`eres arborescentes. Cependant, le syst\`eme de type propos\'e
dans le standard souffre d’impr\'ecisions car il s’appuie sur des approximations
grossi\`eres qui conduisent \`a des analyseurs statiques de type incomplets. En
particulier, les r\`egles de typage propos\'ees pour les it\'erateurs for perdent la
notion d’ordre dans XML et de fa{\c c}on plus importante le type $any$ est
inf\'er\'e d\`es que des axes inverses horizontaux et verticaux de XPath sont utilis\'es.

L’objectif de ce projet de master est de proposer un syst\`eme de type alternatif
plus pr\'ecis pour XQuery. Pour ce faire, nous introduisons une nouvelle notion
de type bidirectionnelle pour les expressions r\'eguli\`eres de type: types
d’arbres \`a focus qui permettent de supporter tous les axes de XPath. Nous
suivons \'egalement les propositions de \cite{colazzo-icfp04} pour le typage statique plus
pr\'ecis des expressions \tt{for}. Nous d\'emontrons les avantages de ce syst\`eme de
type plus pr\'ecis sur le fragment de base du langage XQuery, pour lequel nous
proposons de nouvelles r\`egles de typage qui ne souffrent pas des d\'efauts
mentionn\'es et permettent donc de d\'etecter beaucoup plus d’erreurs de typage.

\section*{Mot de clefs}
    XQuery, XML, Syst\`eme de type, Langages fonctionnels

\tableofcontents
{
 \chapter*{List of Figures and Tables} 
 %\addcontentsline{toc}{chapter}{List of Figures and Tables} 
  \let\chapter\section 
  \makeatletter\let\markboth\@firstoftwo\makeatother 
  \listoffigures 
  \listoftables \newpage~\newpage
}

% empty pages before chapters
\let\origdoublepage\cleardoublepage
\newcommand{\clearemptydoublepage}{%
  \clearpage
  {\pagestyle{empty}\origdoublepage}%
}
\let\cleardoublepage\clearemptydoublepage

\pagestyle{fancy}     % arabic numbering
\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ \sc{#1}}{}}
\renewcommand{\sectionmark}[1]{\markright{\thesection.\ \sc{#1}}{}}

%empty style float pages
\fancyhead{}
\fancyhead[LE]{\iffloatpage{}{\leftmark}}
\fancyhead[RO]{\iffloatpage{}{\rightmark}}
\fancyfoot[C]{\iffloatpage{}{\thepage}}
\renewcommand{\headrulewidth}{\iffloatpage{0pt}{0.4pt}}


\pagenumbering{arabic}

\include{introduction}

\include{body}

\addcontentsline{toc}{chapter}{Bibliography}

\bibliographystyle{alpha}  
    
\bibliography{references}

\include{appendix}

\end{document}
